import MainPanel from './components/MainPanel.vue';
import Test from './components/Test.vue';

const routes = [
    {
        path: '/',
        component: MainPanel,
    },
    {
        path: '/test',
        component: Test,
    },
];

export default routes;
